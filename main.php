<?php
session_start();
?>

<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
	
	<script src="js/jquery-1.11.2.min.js"></script>
	<script src="js/datatable.min.js"></script>
	<link href="css/datatable.min.css" rel="stylesheet" />
	<link href="css/bootstrap.min.css" rel="stylesheet" />
	
</head>
<body>

<center><div class="paging"></div></center>
	<table id="myTable" class="table table-striped">
		<thead>
			<tr>
				<th>region</th>
				<th>channel</th>
				<th>bpname</th>
				<th>agentid</th>
				<th>producttype</th>
				<th>mainplan</th>
				<th>acctno</th>
				<th>netserviceid</th>
				<th>accountname</th>
				<th>activation</th>
				<th>actmonth</th>
				<th>churn</th>
				<th>churnmonth</th>
			</tr>
		</thead>
		<tbody>
		<div id="myLoader"><h1 style="text-align: center; margin-left: auto; margin-right: auto;">Loading Please Wait...</h1><div id="progressContainer"><progress></progress></div><br/><br/></div>
		</tbody>
	</table>
	<center><div class="paging"></div></center>
	
	
	<script>
	  
		  //Setting Up AJAX Defaults - IMPORTANT
		  $(function() {
			$.ajaxSetup({
			  cache: false,
			  timeout: 15000
			});
		  });
	  
	  	  //Document Ready function
		  $(function()
		  {
		  
		  $.getJSON("rptjson.php", function(myData)
         		{
					$.each(myData, function(key, value) {
						$('#myTable').append('<tr>'+
						'<td>'+value['region']+'</td>'+
						'<td>'+value['channel']+'</td>'+
						'<td>'+value['bpname']+'</td>'+
						'<td>'+value['agentid']+'</td>'+
						'<td>'+value['producttype']+'</td>'+
						'<td>'+value['mainplan']+'</td>'+
						'<td>'+value['acctno']+'</td>'+
						'<td>'+value['netserviceid']+'</td>'+
						'<td>'+value['accountname']+'</td>'+
						'<td>'+value['activation']+'</td>'+
						'<td>'+value['actmonth']+'</td>'+
						'<td>'+value['churn']+'</td>'+
						'<td>'+value['churnmonth']+'</td>'+
						'</tr>');
					});
					
					$("#myLoader").fadeOut("slow","swing");
					
					$('#myTable').datatable({
					pageSize: 10,
					sort: [false, true, false],
					filters: [false, false, false, false,false,false,false],
					filterText: 'Type to Search'
					}) ;
					
			
			});
		 });
	</script>
Login Successful
<a href="logout.php">Click to Logout</a>
</body>
</html>